﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraGodview : MonoBehaviour
{

    public Transform playerTransform;
    
    [SerializeField]
    private Vector3 camOffset = Vector3.zero;

    public float SmoothFactor = 0.5f;
    public float RotatioSpeed = 5.0f;
    // Start is called before the first frame update
    void Start()
    {
        camOffset = transform.position - playerTransform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {

        Vector3 newpos = playerTransform.position + camOffset;

        transform.position = Vector3.Slerp(transform.position,newpos, SmoothFactor);

    }
}
