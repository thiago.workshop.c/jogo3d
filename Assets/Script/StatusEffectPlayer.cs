﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffectPlayer : MonoBehaviour
{

    public int multiplyDmgOverTime;
    
    
    // Start is called before the first frame update
    void Start()
    {
        this.multiplyDmgOverTime = 0;
        this.multiplyDmgOverTime = 0;

    }

    // Update is called once per frame
    void Update()
    {
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "NPC")
            multiplyDmgOverTime++;
        
    }
    public void ResetMultiplyDmgOverTime()
    {
        this.multiplyDmgOverTime = 1;
    }
    public void PermantDmgMultiply()
    {
        this.multiplyDmgOverTime++;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "NPC")
            if(multiplyDmgOverTime>1)
                multiplyDmgOverTime--;
    }
}
