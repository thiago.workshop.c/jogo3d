﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsPlayer : MonoBehaviour
{
    private int _peopleSaved;
    private int _maskStored;
    private int _alcoolStored;

    public UIManager uiManager;
    // Start is called before the first frame update
    void Start()
    {
        this._peopleSaved = 0;
        this._maskStored = 0;
        this._alcoolStored = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E)){
            if (_maskStored > 0)
            {
                uiManager.PlayerObject.GetComponent<StatusEffectPlayer>().ResetMultiplyDmgOverTime();
                DecresceMask();
                uiManager.hb.health = 100;
            }
        } 
    }

    public void AddPeopleSaved()
    {
        this._peopleSaved++;

        uiManager.textPeopleNumber.text = this._peopleSaved.ToString();
    }
    public void AddMask()
    {
        this._maskStored = _maskStored + 10;
        this._alcoolStored++;
        uiManager.texCollectAlcool.text = this._alcoolStored.ToString();
        uiManager.textCollectMask.text = this._maskStored.ToString();

    }
    public void DecresceMask()
    {
        this._maskStored--;
        uiManager.textCollectMask.text = this._maskStored.ToString();

    }
    public void DecresceAlcool()
    {
        if(_alcoolStored >0) { 
        this._alcoolStored--;
        }
        else
        {
            this._alcoolStored = 0;
        }
        uiManager.texCollectAlcool.text = this._alcoolStored.ToString();

    }
    public int GetPeopleSaved()
    {
        return this._peopleSaved;
    }
    public int GetMaskStored()
    {
        return this._maskStored;
    }


}
