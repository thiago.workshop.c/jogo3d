﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject cameraCharacter;
    public GameObject cameraGodview;
    private Transform _oldposition;
    void Start()
    {
        desactiveGodView();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            activeGodView();

        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            desactiveGodView();
        }
    }


    void activeGodView()
    {
        cameraCharacter.SetActive(false);
        cameraCharacter.transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z);
        cameraGodview.SetActive(true);
    }
    void desactiveGodView()
    {

        cameraCharacter.SetActive(true);
        cameraCharacter.transform.eulerAngles = new Vector3(180, transform.eulerAngles.y, transform.eulerAngles.z);

        cameraGodview.SetActive(false);
    }
}
